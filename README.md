# Hackintosh: ASUS Maximus Hero XI - AMD Radeon RX 580 8GB - Intel i7-8700K

## Hardware

- Cooler Master HAF XB EVO - High Air Flow Test Bench and LAN Box Desktop Computer Case with ATX Motherboard Support
- Cooler Master MegaFlow 200 - Sleeve Bearing 200mm Silent Fan for Computer Cases (Black)
- 2 of Cooler Master Sleeve Bearing 80mm Silent Fan for Computer Cases and CPU Coolers
- ASUS ROG Maximus XI Hero (Wi-Fi) Z390 Gaming Motherboard LGA1151 (Intel 8th 9th Gen) ATX DDR4 DP HDMI M.2 USB 3.1 Gen2 802.11AC Wi-Fi
- CORSAIR RMX Series, RM750x, 750 Watt, 80+ Gold Certified, Fully Modular Power Supply
- Noctua NH-U9S, Premium CPU Cooler with NF-A9 92mm Fan (Brown)
- Intel Core i7-8700K Desktop Processor 6 Cores up to 4.7GHz Turbo Unlocked LGA1151 300 Series 95W
- XFX - AMD Radeon RX 580 GTS Black Edition 8GB GDDR5 PCI Express 3.0 Graphics Card - Black
- Samsung SSD 970 PRO 512GB - NVMe PCIe M.2 2280 SSD (MZ-V7P512BW)
- WD - Black SN750 NVMe SSD 500GB Internal PCI Express 3.0 x4 (NVMe) Solid State Drive for Laptops
- WiFi + Bluetooth 4.0 Card to PCI-E x1 Adapter Card PC/Hackintosh Without BCM943224PCIEBT2/bcm94360CS2/BCM943602CS (black)
- Padarsey BCM94360CS2 WiFi Bluetooth Airport Wireless Card Compatible for MacBook Air 11" A1465 (2013, 2014, 2015) 13" A1466 (2013, 2014, 2015, 2017) (661-7465, 661-7481, 653-0023)
- G.SKILL 32GB (2 x 16GB) Ripjaws V Series DDR4 PC4-25600 3200MHz for Intel Z170 Platform Desktop Memory Model F4-3200C16D-32GVK
- 2x Chucked WD - Easystore 8TB External USB 3.0 Hard Drive - Black
- 2x Chucked WD - Easystore 12TB External USB 3.0 Hard Drive - Black
- Logitech - MX Master Wireless Laser Mouse - Meteorite
- Logitech - K780 Wireless Keyboard - White
- Samsung - 55" Class - LED - Curved - NU8500 Series - 2160p - Smart - 4K UHD TV with HDR
- Apple - Apple Watch Series 5 (GPS) 44mm Space Gray Aluminum Case with Black Sport Band - Space Gray Aluminum
- DIERYA K1 1TB Professional SSD 3D NAND SATA III 6Gbps 2.5" Internal Solid State Drives for PC Laptop